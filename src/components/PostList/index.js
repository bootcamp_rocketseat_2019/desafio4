import React, { Component } from "react";

import { Container, Comments } from "./styles";

class PostList extends Component {
  state = {
    posts: [
      {
        id: 1,
        author: {
          name: "João",
          avatar:
            "https://cdn2.vectorstock.com/i/1000x1000/20/76/man-avatar-profile-vector-21372076.jpg"
        },
        date: "10 Ago 2019",
        content:
          "Boa noite, amigos! Esse é um projeto que estou fazendo no bootcamop da Rocketseat!",
        comments: [
          {
            id: 1,
            author: {
              name: "César",
              avatar:
                "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABLFBMVEX////9yI7/T09iPzP2tHvyP0RzSj7ioHHWNDr5tnz6SUv4uYBrQzrPlmn2tXz/SEz3p3VYNi72sHL/QUL86dvyOkLnrof4wpZVMiv/zpLxMTf9x41dNypwRjr9xIXyuILjsX/lp3PkOj/UKDVZMSL7fmNVKxr1hGS2imT83dvzZ1X/x8XialT/gn7dNzzEl23usX1rPjDxKjH1lWvqi2Xzv4jZQkFUJQ362Lx2Uj/crHv85dP+9e791ayieVn3mpjzT1DwExz9z536tbXdV0rumWzlionfaGfbVFf2iIrTHSj96+vo4+H0amyhjojTycXieHb41sj0XF77zs3+rqr/m5j4o6H1dHTUIyuNdW2IZFfCtrOmjYSbh4Hf1tK0n5h1V01wV1OLZEuWdGft8o0QAAAMK0lEQVR4nO2deUMaSRPG5RoRDAaViCAQ4hGMqxISPBIkbtxcSmQ37sZVVmF9v/93eOdgmJme7unuoasbXZ8/ZY76UdVVXd0zMjUlR61ut3d+fnOtlWKxlRvm0zrdbrd/fn5+rcVirwHNG0edbv9mMNjYWFtbW1lZiZna71DOue32+9cDbcM+zThPK/0jx2IOdXo3g33dxiGXo5Ur0hm3vfMrbV8HG30btjRNe30r1XyKOr2rlQ0/21AbfifqbIOYiYY9QwfUStcKQPDq3QTQ+Z142zP8FniCZup1VxWRR72rYGNN2SOx1T0frNGPtwAnwom3N5hhR3LibX+wz3S0Zkv5SOxpGywGG9ro3WywHjwC1Er/U8sXW2PEM8TkPARQdyKlzkCqW+LhY9es5iHsq+LrXG2A8MVKmlclRYC9feaY45OGSlHBgHKgH1DN1K2jATkQA6gkTDvsWZFPszhABSWxA5NC/TlGWTYF4sNGqBmlsmdu17KSqONEuYA9mCyKH4LSB6LevfdB+EhDcEjYk4OnNxHEbnVcBfHJqoi9GHMTIRpQSqrpzELNYWLBQ9Ai1MAB+/twfDQHmgOxBQwINgllBATvEa+g5jAxSg51CGHbi3NAQCY+aMIu4BhkBAQuiHA1gi1CTULIuXcfLEaZ+YBLPlSMUougLEIoF/LwwRIOQPjYR+CQkH37kVcdkFrPyacTkvbmxlcPIEh5HaiBLu3fiK8V/HygzYVwF4ZwICyh6GEYig+SUHCiCedADXIciiUMywdJKDKVhueDrIfiCEMH6H0hHIsPctYmiHA8B4IS3orINFxdhGxCEbl0fD7IDnh8QhF8kKsYrTEJxx6ANiHcStRYmUYUHyhhKZghG6DYLJ+Cvg9AwqDuKZs9bNeeiNHubq19SKYE3EA8JxFm83dP4jPjK50uFyOGlpeXm7t3BEbAfYsuYSBm29WZmfg4yozYXFp+3sZWT0BCfMnPHo7Dp7MViz64EeMhhhFyIx+3XJpvh+Qz2QhojvxuLA0ACWcxgDV+vjQT29CNNRQRdA/Y/2gJH2A6KCRZEUGfou2jhNk2GyCP13yIbYmPmPr6pzsKYDqM13w6lEeIzr2zdQJhRgiZreceJ8JuH3qTaRYdhBkxPkPljVPYZ6I0rw+9ESkazFHTQwj6oIJnZoqkmTQYoNeJsE/ueVJNviqLMPJOGqFn3nboHYWQhBFnDg780Je7zUdrISThcnuECP3gnmsXOPtEIqEzsSkBPwXtSjXZaYlRujsihH681JVqslWJhE6qgX6lxJVqpBI60xrwd7v21RA6NR/82cvBilrC0iEwoGs1ShEh+DuWTqpRQwj/yozTQCkihH8XQXGmkfA+ydVwIGbzcqtFSVIqHT6faKzht+txiT5sG+v84J2FKb3mZ2M1Yw0/LpHQXOevzc5KeV19P9uO45ZnYAlNykh7RcYrQf8+wS8/pT22hCOgHlGT8VrXM8L6mkPY3K29CwH4rrbbpB50AA/4fh4P6BC+y2az+UNuwMO8fh71mykvghMS14BtwmbWLCZ3fJG6fGedRvciNOAByYUjwt2stU5Ft9WtZt6qsru0A8vvgQkzJECbcLk2JHzORfh8SFijub74ERawRXShLMJIGXZWQ8wzEglhcw15GMojhB2Ik0AIWxLpURp5Z9ka4wKMRKyT8vSpArAPF+mEVmXL73LWw908YxUFzjRTdMJIpB3La5yABqKWj7UZDizDAk4dUec0keJypBhi7m2eRd+DLB4BE5ILoqu3CLtXynIedJBOTS1RZ96gKkO7UFea2j0BCnrOZqqVoXfAYIDgnYWFiA9UCYRlGR40dZCZ90MCExaL5Qh04+TW4sES2kcBE348gO/uUVXkEUpJMI+Ej4T3kXBJ3s7Mf4BQwlwNRygxStUQoisaeOMqzaCOodissBACr1yQhK5o4Egq07rqRMvrxscMjBLW8nFCW0UMoQloQODoi6NP6YTwO79YpakDsTptq4pAFiuuz2iAilIpy0Cc9qhar1d01ev1qvcDqgvVDEN/mJYDfBgkqg9VBamvXmR8pjWZCGm7VIpqhSHUif6RWKHz0RONOhf6pjWYdFqkBWqVusCm0IV+J/rjVI/UIMYqwz5qUSGgv+pjp27uyuDBw5ZJVIqq/UhHLIgGJFIhqnUmPIWVYiR04S1gAl7UZ6GGAmeqKKDKQciPyK9JAGQO1FCAykPUEjp7E4VYBH+yhFnoKrgQxGL5SGGl92mxMu8u/ri6yMv3UXGV8GnxyLPSP97rluXJ8p+t1kF8XgRisSzjCcSQWlxyHBkSUR9+kxaeXrUORowh8k2xXD6YxPBE9N7OOryIenaZmPJAkR2sXK93T2h2Ial1YO2FcyBObnYh6RlPvmkuqTY3hJ5NT9czbIOxMl29p4R6G6jPbyhubNb1w2buLaHJGIRotv/V+H0mNABIkdqs21/CPSfU5V+zKFbq0yMvPwBCa2nGXNQ31/VdizYG4AMhJKwnxh844XAO+3AJ4w+csBp/4ISuJYH7RfjzOHG5SSesxhHCzcvE8U/VxjNoM1XIJXKNCwqhh0/X0tRFQz+vkNpUDUDTB91OQ7kWa4AOV+emhuc1PqhGCNTmcSFhKfeKvKmG+s9sli9zwzMLxxPsRtuBJuJbDr54+o3rzIl1o+NAS8x4eozGPWdOqBtbv+Y8ZvrjlIBnuPAVcu6vk7hks5VKeOWO0yqZDolRS6kt1TiotrYbCyhhIhEnvzUcEKMm4UJje4Igt7YXFuai0ShqZu5Vmo6HiVFd+tXmFhYmA9LGi+IQ37Ag+mM0MbzeBEC68TCETHGKidGEc0mlkAge1okMcYqP0ahySAwezon0OMXEaMJ3XemQeLxQcRoco2ogz/4i4WGdSIlTTIziCS3Iv87gAf8k44WIU6YYdWlh4U9gvtaPhSADcGEaFKe4GA0k1Bl/gE7ozqKBDsQ7MSBOeWLU1lwUMFJbdECME8lxiotRKqFuAxzhdzogT5yGiFEL8RIK8KTBcn+MEwlxiotRpjs0ToAIC4wG+DS3jkNMr8+FA9RbZBjAkwKbCT6rU1Ecog4Y9TdcbJcvwDjxOGeZy42o/8mPaABiD6XI/FJyPyAAzxq2RzgJzb+hiBYg/lgqYCLRgKgYW/Y6Eyfi8HAvog2IxCkrYKIAMUX9UEBsZiMcHexGHAEiiKyAiQLEguO3HLMhUazROqKtdXzbzH7d3DcAwuMcuyn4I+fWX1pan6MfHHzZ3DEAIdeAwQf0XM6Sd2qU4gfUJR7wzLugzWYMMmJTwX/mASyIT6Z2scAbSTAH+SOBEH8wKm/WBSgXF15CGiLWZhIh/uggwETjQjjhiTdKWRB9RxAJU7yAEPO2nyghxSTcV0AkND6hXQ4hFL8ffsnbjWM+JhPijg4CTOTE94jfw/VyrIScgIncd+GEviANgRiWEHdr8S1iA3cbTsSQhNg7N0QDbuIJ+WwNR+hvk01C0RvhaDkMhRiKEA8oviD6ymEYxDCEBEDxBfEDiZBnKIYhJN1WeIf4DVMsuBFDEBLvKrxDPCYTsiPyE5JvKrxDPCbfi91gbkLSIDQkmPCMOAx5nMhNGHRTwR3iGaFY8FnMSxjkQtEd4kWgD1mdyEsYeM+C2IJILIeQhIEuFF0QyeUwjMliCMUWxO2AYgFGGHjLRG5bKCGuO3RLBaHQDjG4WKghFFkuWnunyeTOzosXLyaEULdkZyeZPN0T9VjG57+TbhmoKKsMQuO2Ox5L/v4siPDLahInt1chCYcew2j1iyDCr9jLe7zKZnKU6/uIvsBzufRVECH1Rskko80pHhdG6bfdEUR4Sr8To806Y4q9daJ/s6eCCH8/XcWPRG4f8olyz9XV098FEU6dfP7yNXmqcxJJZRIaZpyeJr9++Sx6qeaPN29f/vLb3qol6YTD++799svLt2/+EMxmaXE+Y27Cx59+MlGTDuoeCOGeA5Y0wT49jZsWZOZh/kmP/UtIGV0mqYMK5EMHzCQzbmyZAEzoPFs4RE3HP7E8tMiruU9Dj43ARpJF6KA+BSF8Sny4+JHwkfCR8JHw3hKSf3JNPiHQOxcTRAgD6Ptns8oI56H+txsxTGUTzgABknONZEKgPGOqlZ7H/XCeVML5CvBPHvt+b00uYeaI999j/h8Fd2Y3Alfk8AAAAABJRU5ErkJggg=="
            },
            content: "Muito legal!"
          },
          {
            id: 2,
            author: {
              name: "Ana",
              avatar:
                "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS8_n7wpMAE-b7Jvm6fPw3AK39wKOBJqdcRSErJcD-GHzoGr51b"
            },
            content: "Bacana!!"
          }
        ]
      },
      {
        id: 2,
        author: {
          name: "Paulinha",
          avatar:
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ97sd4BlD0Y6FkQrjsGqD1Rix0imQNvjkLdH8Le7je3yjb36MG"
        },
        date: "11 Ago 2019",
        content:
          "Ô, Sol vê se não esquece e me ilumina. Preciso de você aqui. Ô, Sol" +
          " vê se enriquece a minha melanina. Só você me faz sorrir",
        comments: [
          {
            id: 1,
            author: {
              name: "Bianca",
              avatar:
                "https://cdn5.vectorstock.com/i/1000x1000/73/04/female-avatar-profile-icon-round-woman-face-vector-18307304.jpg"
            },
            content: "Curto essa música!"
          },
          {
            id: 2,
            author: {
              name: "Rui",
              avatar:
                "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTynSetkTudpJGPUFVGN6yT5jNS7D8JLD2bOBj6HqiYKlJ6QhKR"
            },
            content: "Nice!"
          }
        ]
      },
      {
        id: 3,
        author: {
          name: "Billy",
          avatar:
            "https://www.nationalgeographic.com/content/dam/animals/thumbs/rights-exempt/mammals/d/domestic-dog_thumb.jpg"
        },
        date: "12 Ago 2019",
        content: "Gosto de bife ancho!",
        comments: [
          {
            id: 1,
            author: {
              name: "Mog",
              avatar:
                "https://www.petmd.com/sites/default/files/Acute-Dog-Diarrhea-47066074.jpg"
            },
            content: "Prefiro t-Bone"
          },
          {
            id: 2,
            author: {
              name: "Barty",
              avatar:
                "https://www.guidedogs.org/wp-content/uploads/2018/01/Mobile.jpg"
            },
            content: "Eu curto shoulder steak!"
          },
          {
            id: 3,
            author: {
              name: "Bob",
              avatar:
                "https://cdn.images.express.co.uk/img/dynamic/1/590x/dog-650299.jpg"
            },
            content: "Amarro-me em uma prime rib!"
          }
        ]
      },
      {
        id: 4,
        author: {
          name: "Grek",
          avatar:
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTiJYlmXu2ALgUcZfTv_QSRfZTltpgrzub-YnFxMSOAHwQaVWau"
        },
        date: "15 Ago 2019",
        content:
          "Procuro desenvolvedores react e nodejs para projeto!! Mandar email para <tavares@teste.com>",
        comments: [
          {
            id: 1,
            author: {
              name: "Nina",
              avatar:
                "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSWeaRZwkxCiNrfbDpmpRSlXL1jFnAv3nBRUnUl4qeshoE3jWSG6Q"
            },
            content: "Bom dia! Tenho interesse! Te mandei minhas informações"
          },
          {
            id: 2,
            author: {
              name: "Tavares",
              avatar:
                "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT3WF0jvGOEOSLTIbZbxGGsHwotgUEMioaKpLyELi-tksaZ9dvC"
            },
            content: "Te passei meus contatos e informações! Obrigado!"
          }
        ]
      }
    ]
  };

  render() {
    const { posts } = this.state;

    return (
      <Container>
        <ul>
          {posts.map(post => (
            <li key={post.id}>
              <div className="profile">
                <img src={post.author.avatar} />
                <div className="datas">
                  <h3>{post.author.name}</h3>
                  <p>{post.date}</p>
                </div>
              </div>
              <div className="post">
                <p>{post.content}</p>
              </div>
              <div className="linhaDivisoria" />
              <Comments>
                <ul className="commentsUl">
                  {post.comments.map(comment => (
                    <li key={post.comments.id} className="commentsList">
                      <img src={comment.author.avatar} />
                      <div className="contetComment">
                        <strong>{comment.author.name}</strong>
                        <p>{comment.content}</p>
                      </div>
                    </li>
                  ))}
                </ul>
              </Comments>
            </li>
          ))}
        </ul>
      </Container>
    );
  }
}

export default PostList;
