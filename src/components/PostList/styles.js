import styled from "styled-components";

export const Container = styled.div`
  height: 100%;
  width: 100%;

  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  padding-top: 65px;

  ul li {
    display: flex;
    flex-direction: column;
    background: #fff;
    border-radius: 4px;
    width: 700px;
    margin: 20px;
    box-shadow: 0px 1px 2px #ddd;

    div.profile {
      display: flex;
      flex-direction: row;
      padding: 20px;

      img {
        border-radius: 50px;
        width: 40px;
        height: 40px;
        margin-right: 10px;
      }

      div.datas {
        display: flex;
        flex-direction: column;

        p {
          font-size: 11px;
          color: #979797;
          font-weight: bold;
        }
      }
    }

    div.post {
      display: flex;
      flex-direction: row;
      padding: 0 20px 20px 20px;
    }

    div.linhaDivisoria {
      margin: 0 20px 0;
      border-bottom: 1px solid rgb(236, 236, 236);
    }
  }
`;

export const Comments = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;

  ul.commentsUl {
    padding-top: 20px;
    padding-bottom: 10px;

    li.commentsList {
      display: flex;
      padding: 0 20px 0;
      flex-direction: row;
      border-radius: 0;
      margin: 0 0 10px;
      box-shadow: none;

      img {
        border-radius: 50px;
        width: 40px;
        height: 40px;
        margin-right: 10px;
      }

      div.contetComment {
        margin: 0;
        display: flex;
        flex-direction: row;
        background: rgb(236, 236, 236);
        width: 100%;
        padding: 15px;
        border-radius: 15px;
        font-size: 13px;

        strong {
          margin-right: 6px;
        }
      }
    }
  }
`;
