import React, { Component } from "react";

import { FaUserCircle } from "react-icons/fa";
import logo from "../../assets/images/facebook.png";
import my_profile from "../../assets/images/my_profile.png";
import { Container } from "./styles";

class Header extends Component {
  render() {
    return (
      <Container>
        <img alt="facebook" src={logo} />
        <div>
          <img alt="profile" src={my_profile} />
          <FaUserCircle />
        </div>
      </Container>
    );
  }
}

export default Header;
