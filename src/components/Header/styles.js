import styled from "styled-components";

export const Container = styled.div`
  height: 64px;
  background: #4a90e2;
  position: fixed;
  width: 100%;

  padding: 0 50px;

  display: flex;
  align-items: center;
  justify-content: space-between;

  div {
    display: flex;
    align-items: center;
    justify-content: space-between;
    svg {
      color: #fff;
      font-size: 30px;
      margin-left: 10px;
    }
  }
`;
