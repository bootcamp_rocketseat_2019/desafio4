import React from "react";

import GlobalStyles from "./styles/global";

import Header from "./components/Header";
import PostList from "./components/PostList";

export default function App() {
  return (
    <>
      <Header />
      <PostList />
      <GlobalStyles />
    </>
  );
}
